// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "DagNodeBinaryCodable",
    platforms: [
        .macOS(.v10_15),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "DagNodeBinaryCodable",
            targets: ["DagNodeBinaryCodable"]
        ),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(
            name: "BimutableHashKVStore",
            url: "https://gitlab.com/novoco-open-libs/bimutable-hash-kv-store.git",
            .branch("master")
        ),
        .package(
            name: "BinaryCodable",
            url: "https://github.com/jverkoey/BinaryCodable.git",
            .upToNextMinor(from: "0.3.1")
        ),
        .package(
            name: "SwiftMultihash",
            url: "https://gitlab.com/novoco-open-libs/swift-multihash.git",
            .branch("master")
        ),
        .package(
            name: "Flynn",
            url: "https://github.com/novocodev/flynn.git",
            .branch("master")
        ),
        .package(
            name: "SwiftTasks",
            url: "https://gitlab.com/novoco-open-libs/swift-tasks.git",
            .branch("master")
        ),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "DagNodeBinaryCodable",
            dependencies: ["BinaryCodable", "SwiftMultihash", "Flynn", "BimutableHashKVStore"]
        ),
        .testTarget(
            name: "DagNodeBinaryCodableTests",
            dependencies: ["DagNodeBinaryCodable", "BimutableHashKVStore"]
        ),
    ]
)
