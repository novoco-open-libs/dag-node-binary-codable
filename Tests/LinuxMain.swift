import XCTest

import DagNodeBinaryCodableTests

var tests = [XCTestCaseEntry]()
tests += DagNodeBinaryCodableTests.allTests()
XCTMain(tests)
