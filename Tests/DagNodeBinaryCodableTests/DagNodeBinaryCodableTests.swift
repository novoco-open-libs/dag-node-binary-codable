import XCTest
import BimutableHashKVStore

@testable import DagNodeBinaryCodable

final class DagNodeBinaryCodableTests: XCTestCase {
    func testStoreDagNode() {
        XCTFail()
    }
    
    func testStoreAndRetrieveDagNode() {
        //let dg = DagNodeBinaryCodable()
        
        XCTFail()
    }
    
    func testStoreDagNodeGraph() {
        XCTFail()
    }

    func testStoreAndRetrieveDagNodeGraph() {
        XCTFail()
    }
    
    func testStoreAndModifyDagNodeGraph() {
        XCTFail()
    }
    
    func testListenForChangesOnDagNodeGraph() {
        XCTFail()
    }
    
    static var allTests = [
        ("testStoreDagNode", testStoreDagNode),
        ("testStoreAndRetrieveDagNode", testStoreAndRetrieveDagNode),
        ("testStoreDagNodeGraph", testStoreDagNodeGraph),
        ("testStoreAndRetrieveDagNodeGraph", testStoreAndRetrieveDagNodeGraph),
        ("testStoreAndModifyDagNodeGraph", testStoreAndModifyDagNodeGraph),
        ("testListenForChangesOnDagNodeGraph", testListenForChangesOnDagNodeGraph),
    ]
}
