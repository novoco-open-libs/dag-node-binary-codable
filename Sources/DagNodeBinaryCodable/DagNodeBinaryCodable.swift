import Flynn
import SwiftMultihash
import BimutableHashKVStore

public final class DagNodeBinaryCodable: Actor, DagNodeCodable {
    public var safeState: HashedBlockStore = InMemoryHashedBlockStore()
}
