import SwiftMultihash

public struct DagNode {
    private let links: [Multihash]
    private let data: [UInt8]
}
